#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include "vector.h"

#define SECTION_MAP_PATH "/home/db/section_map"

static struct pam_conv login_conv = {
    misc_conv,
    NULL
};

void print_usage() {
    printf("USAGE ./app [Options] [Args]\n");
    printf("Options:\n");
    printf("\t1. sections - prints all sections\n");
    printf("\t2. loans [section] - prints all loans for given section, if section is not given prints loans not contained within any section \n");
    printf("\t4. set [loan] [section] - changes loan section to given section\n");
    printf("\t5. help - get help\n");
}

int print_sections() {
    FILE *fp = fopen(SECTION_MAP_PATH, "r");
    if (fp == NULL)
        return -1;
    
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    Vector *sections = Vector_new(free);


    while((read = getline(&line, &len, fp)) != -1) {
        int spaces = 0;
        int l_space = 0;
        for (int i = 0; i < read; i++) {
            if (line[i] == ' ') {
                spaces++;
                l_space = i;
            }
        }
        if (spaces > 0) {
            char *sect = malloc(sizeof(char) * read - l_space - 2);
            memcpy(sect, line + l_space + 1, read - l_space - 2);
            sect[read - l_space - 2] = 0;
            Vector_add(sections, sect);
        }
    }
    if (line)
        free(line);

    Vector *unique = Vector_new(NULL);
    int new_elem = 1;

    for (int i = 0; i < Vector_getSize(sections); i++) {
        char *curr = Vector_getElemById(sections, i);
        for (int j = 0; j < Vector_getSize(unique); j++) {
            if (strcmp(curr, Vector_getElemById(unique, j)) == 0) {
                new_elem = 0;
                break;
            }
        }
        if (new_elem == 1)
            Vector_add(unique, curr);
    }

    for (int i = 0; i < Vector_getSize(unique); i++)
        printf("%s\n", Vector_getElemById(unique, i));

    Vector_remove(sections);
    Vector_remove(unique);
    
    return 0;
}

int print_loans(char *section) {
    FILE *fp = fopen(SECTION_MAP_PATH, "r");
    if (fp == NULL)
        return -1;
    
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    Vector *loans = Vector_new(free);

    while((read = getline(&line, &len, fp)) != -1) {
        int spaces = 0;
        int l_space = 0;
        for (int i = 0; i < read; i++) {
            if (line[i] == ' ') {
                spaces++;
                l_space = i;
            }
        }
        if (spaces == 0 && section == NULL) {
            char *loan = malloc(sizeof(char) * read-1);
            memcpy(loan, line, read-1);
            loan[read-1] = 0;
            Vector_add(loans, loan);
        } else if (section != NULL && spaces == 1 && strncmp(section, line + l_space + 1, read - l_space - 2) == 0) {
            char *loan = malloc(sizeof(char) * l_space);
            memcpy(loan, line, l_space);
            loan[l_space] = 0;
            Vector_add(loans, loan);
        }
    }

    for (int i = 0; i < Vector_getSize(loans); i++) {
        printf("%d. %s\n", i+1, (char*)Vector_getElemById(loans, i));
    }
    Vector_remove(loans);

    return 0;
}

int set_loan_section(char *loan, char *section) {
    FILE *fp = fopen(SECTION_MAP_PATH, "r");
    if (fp == NULL)
        return -1;
    
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    Vector *loans = Vector_new(free);
    Vector *sections = Vector_new(free);

    while((read = getline(&line, &len, fp)) != -1) {
        int spaces = 0;
        int l_space = 0;
        for (int i = 0; i < read; i++) {
            if (line[i] == ' ') {
                spaces++;
                l_space = i;
            }
        }
        int end;
        if (spaces == 0) {
            char *loan = malloc(sizeof(char) * read-1);
            memcpy(loan, line, read-1);
            loan[read-1] = 0;
            Vector_add(loans, loan);
            Vector_add(sections, NULL);
        } else {
            char *loan = malloc(sizeof(char) * l_space);
            memcpy(loan, line, l_space);
            loan[l_space] = 0;
            Vector_add(loans, loan);
            char *sect = malloc(sizeof(char) * read - l_space - 2);
            memcpy(sect, line + l_space + 1, read - l_space - 2);
            sect[read - l_space - 2] = 0;
            Vector_add(sections, sect);
        }
    }
    if (line)
        free(line);

    int found = 0;
    for (int i = 0; i < Vector_getSize(loans); i++) {
        if (strcmp((char*)Vector_getElemById(loans, i), loan) == 0) {
            char *new_sect = Vector_getElemById(sections, i);
            if (new_sect != NULL)
                free(new_sect);

            new_sect = malloc(sizeof(char) * strlen(section) + 1);
            memcpy(new_sect, section, strlen(section) + 1);
            sections->elementsArr[i] = new_sect;
            found = 1;
            break;
        }
    }

    if (found == 1) {
        printf("changed loan '%s' section to '%s'\n", loan, section);
    } else {
        printf("no loan '%s' found\n", loan);
    }


    fclose(fp);
    fp = fopen(SECTION_MAP_PATH, "w");
    if (fp == NULL) {
        return -1;
    }
    
    for (int i = 0; i < Vector_getSize(loans); i++) {
        if (Vector_getElemById(sections, i) == NULL) {
            fprintf(fp, "%s\n", Vector_getElemById(loans, i));
        } else {
            fprintf(fp, "%s %s\n", Vector_getElemById(loans, i), Vector_getElemById(sections, i));
        }
    }
    fclose(fp);
    Vector_remove(loans);
    Vector_remove(sections);


    return 0;
}

pam_handle_t *authorize_worker() {
    pam_handle_t *pamh = NULL;
    int retval;
    char *username = NULL;

    retval = pam_start("loan_service", username, &login_conv, &pamh);
    if (pamh == NULL || retval != PAM_SUCCESS) {
        fprintf(stderr, "Error when starting: %d\n", retval);
        exit(1);
    }
    retval = pam_authenticate(pamh, 0);
    if (retval != PAM_SUCCESS) {
        pam_end(pamh, PAM_SUCCESS);

        fprintf(stderr, "authenticating error: %d\n", retval);
        exit(1);
    }
    return pamh;
}

pam_handle_t *authorize_director() {
    int retval;
    pam_handle_t *pamh = NULL;
    char *username = NULL;

    retval = pam_start("loan_management", username, &login_conv, &pamh);
    if (pamh == NULL || retval != PAM_SUCCESS) {
        fprintf(stderr, "Error when starting: %d\n", retval);
        exit(1);
    }

    retval = pam_authenticate(pamh, 0);
    if (retval != PAM_SUCCESS) {
        pam_end(pamh, PAM_SUCCESS);
        fprintf(stderr, "Error when authenticating: %d\n", retval);
        exit(1);
    }
    return pamh;
}


int main(int argc, char **argv) {
    pam_handle_t *pamh = NULL;

    if ((argc == 2) && (strcmp("help", argv[1]) == 0)) {
        print_usage();
    } else if (argc == 2 && strcmp("sections", argv[1]) == 0) {
        if (!(pamh = authorize_worker())) {
            printf("Authorization failed!");
            return 0;
        }
        print_sections();
    } else if (argc == 2 && strcmp("loans", argv[1]) == 0) {
        if (!(pamh = authorize_worker())) {
            printf("Authorization failed!");
            return 0;
        }
        print_loans(NULL);
    } else if (argc == 3 && strcmp("loans", argv[1]) == 0) {
        if (!(pamh = authorize_worker())) {
            printf("Authorization failed!");
            return 0;
        }
        print_loans(argv[2]);
    } else if (argc == 4 && strcmp("set", argv[1]) == 0) {
        if (!(pamh = authorize_director())) {
            printf("Authorization failed!");
            return 0;
        }
        set_loan_section(argv[2], argv[3]);
    } else {
        printf("Invalid arguments, use help for more info.\n");
    }

    if (pamh)
        pam_end(pamh, PAM_SUCCESS);

    return 0;
}