cd ../zad1/part1 ; python3 -m src.main # run part1
cd ../../zad2

echo $(pwd)

cp pam_conf/loan_service /etc/pam.d/
cp pam_conf/loan_management /etc/pam.d/

rm -rf /home/db
mkdir /home/db
touch /home/db/section_map
touch /home/db/sections

echo mortgage >> /home/db/sections

for i in `seq 10`
do
    if [ $i -gt 5 ]
    then
        echo kredyt$i.txt mortgage >> /home/db/section_map
    else
        echo kredyt$i.txt >> /home/db/section_map
    fi
done

gcc app.c vector.c -o app -lpam -lpam_misc