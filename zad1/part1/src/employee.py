from dataclasses import dataclass
from typing import List


BOSS = 'dyrektor'
WORKER = 'obsługa'
BIZ_DEPT = 'DBB'
SER_DEPT = 'DBD'


class Group:
    BUSINESS = BIZ_DEPT
    SERVICE = SER_DEPT

    @staticmethod
    def all() -> List[str]:
        return [Group.BUSINESS, Group.SERVICE]


@dataclass
class Employee:
    id: int
    name: str
    surname: str
    role: str
    dept: str

    def album(self) -> str:
        return f"{self.name[0]}{self.surname[0]}{self.id}"

    def group(self) -> str:
        return self.dept
