from .employee import Employee
from os.path import join
from typing import List
from .acl_utils import create_sys_users_groups
from .loans_utils import create_loans_dir
from .deposits_utils import create_deposits_dir
from .tasks_utils import create_tasks_dir


class Dirs:
    LOANS = 'kredyty'
    DEPOSITS = 'lokaty'
    TASKS = 'zadania'

    def all(self) -> List[str]:
        return [
            Dirs.LOANS,
            Dirs.DEPOSITS,
            Dirs.TASKS
        ]


def get_file_data(fname: str) -> List[Employee]:
    with open(fname, 'r') as f:
        return [Employee(*line.split()) for line in f]


def create_dirs(root_path: str, employees: List[Employee]):
    create_loans_dir(join(root_path, Dirs.LOANS), employees)
    create_deposits_dir(join(root_path, Dirs.DEPOSITS), employees)
    create_tasks_dir(join(root_path, Dirs.TASKS), employees)


if __name__ == '__main__':
    employees = get_file_data('/root/BSK/zad1/przykladowy.txt')
    create_sys_users_groups(employees)
    create_dirs('/home', employees)
