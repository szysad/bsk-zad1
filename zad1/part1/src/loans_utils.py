from .acl_utils import mkdir, modify_permissions
from .employee import Group, Employee, BOSS
from os.path import join
from typing import List


def create_loans_dir(dirpath: str, emps: List[Employee]):
    mkdir(dirpath, '-p')

    # set --- for others, owner group, owner perm
    for target in ('other', 'user', 'group'):
        modify_permissions(dirpath, (target, None), False, False, False, '-m')

    # set default rwx for user owner
    modify_permissions(dirpath, ('user', None), True, True, True, '-d', '-m')

    # set default --- for group owner
    modify_permissions(dirpath, ('group', None), False, False, False, '-d', '-m')

    # service workers
    for gname in Group.all():
        modify_permissions(dirpath, ('group', Group.SERVICE), True, (gname == Group.SERVICE), True, '-m')
        modify_permissions(dirpath, ('group', Group.SERVICE), True, False, True, '-d', '-m')

    # set default and not r-x for bosses
    for emp in emps:
        if emp.role == BOSS:
            modify_permissions(dirpath, ('user', emp.album()), True, False, True, '-d', '-m')
            modify_permissions(dirpath, ('user', emp.album()), True, False, True, '-m')
