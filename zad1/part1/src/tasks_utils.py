from .employee import Employee, BOSS, WORKER, Group, BIZ_DEPT
from typing import List, Tuple
from .acl_utils import mkdir, modify_permissions
from os.path import join


def create_task_subdir(
        taskdir: str,
        emp: Employee,
        bosses: Tuple[Employee, Employee]
        ):

    emp_dir = join(taskdir, emp.id)
    mkdir(emp_dir, '-p')

    modify_permissions(emp_dir, ('user', emp.album()), True, False, True, '-m')
    modify_permissions(emp_dir, ('user', emp.album()), True, False, True, '-d', '-m')

    if emp.dept != bosses[0].dept:
        bosses = bosses[::-1]
        # now bosses[0] is in same dept as emp

    modify_permissions(emp_dir, ('user', bosses[0].album()), True, True, True, '-m')
    modify_permissions(emp_dir, ('user', bosses[0].album()), True, True, True, '-d', '-m')


def create_tasks_dir(dirpath: str, emps: List[Employee]):
    mkdir(dirpath, '-p')

    bosses = list(filter(lambda emp: emp.role == BOSS, emps))

    # set --- for others, owner group, owner perm and default
    for target in ('other', 'user', 'group'):
        modify_permissions(dirpath, (target, None), False, False, False, '-m')
        modify_permissions(dirpath, (target, None), False, False, False, '-d', '-m')

    for gname in Group.all():
        modify_permissions(dirpath, ('group', gname), True, False, True, '-m')

    for boss in bosses:
        modify_permissions(dirpath, ('user', None), True, True, True, '-d', '-m')
        modify_permissions(dirpath, ('user', boss.album()), True, True, True, '-m')

    for emp in emps:
        if emp.role == WORKER:
            create_task_subdir(dirpath, emp, bosses)
