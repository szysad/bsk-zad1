import subprocess
from os.path import exists
from typing import Tuple, List
from .employee import Group, Employee
from os.path import join
import os


def acl_target_str(target: str, target_name: str) -> str:
    ''' returns "target:target_name" or "target:" '''

    return target + (f":{target_name}" if target_name else ":")


def acl_permissions_str(r: bool, w: bool, x: bool) -> str:
    perm = ['-'] * 3
    if r:
        perm[0] = 'r'
    if w:
        perm[1] = 'w'
    if x:
        perm[2] = 'x'
    return ''.join(perm)


def modify_permissions(
        path: str,
        target: Tuple[str, str],  # target, targetname
        r: bool, w: bool, x: bool,
        *flags: str):

    if not exists(path):
        raise Exception(f"relative path '{path}' is not directory nor file")

    setting_str = f"{acl_target_str(target[0], target[1])}:{acl_permissions_str(r, w, x)}"
    subprocess.run(["setfacl", *flags, setting_str, path])


def mkdir(path: str, *flags: str):
    subprocess.run(["mkdir", ' '.join(flags), path])


def gen_ssh_key(emp: Employee):
    with open(os.devnull, 'w') as devnull:
        subprocess.run(["mkdir", '-p', f'/home/{emp.album()}/.ssh'])
        subprocess.run(['ssh-keygen', '-N', emp.album(), '-f', f'/home/{emp.album()}/.ssh/id_rsa', '-C', f'ssh key of {emp.album()}'], stdout=devnull)



def create_sys_users_groups(employees: List[Employee]):
    with open(os.devnull, 'w') as devnull:
        for gname in Group.all():
            subprocess.run(['groupadd', gname], stderr=devnull)

        for emp in employees:
            subprocess.run(['userdel', emp.album(), '-fr'], stderr=devnull)
            subprocess.run(['useradd', emp.album(), '-g', emp.group(), '-m'])
            ps = subprocess.Popen(['echo', f'{emp.album()}:{emp.album()}'], stdout=subprocess.PIPE)
            output = subprocess.Popen(['chpasswd'], stdin=ps.stdout)
            gen_ssh_key(emp)
            print(f'employee "{emp.album()}" account created')
