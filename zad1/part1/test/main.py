import unittest
from .tests.test_a import TestA
from .tests.test_b import TestB
from .tests.test_c import TestC
from .tests.test_d import TestD
from .tests.test_e import TestE
from .tests.test_f import TestF
from .tests.test_g import TestG


if __name__ == '__main__':
    unittest.main()
