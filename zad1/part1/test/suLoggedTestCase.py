import unittest
import subprocess


class SuLoggedTestCase(unittest.TestCase):

    def run_cmd_logged_as(self, cmd: str, username: str):
        proc = subprocess.Popen(
            ["su", username],
            shell=False,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding='UTF-8'
        )
        rez = proc.communicate(cmd)
        return tuple(map(str.strip, rez))
