from src.employee import Employee, BOSS, SER_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from .test_a import TASK1_CONTENT, TASK1_FNAME

# 1 Ada Abacka dyrektor DBD
DBD_boss, = list(filter(
        lambda emp: emp.role == BOSS and emp.dept == SER_DEPT,
        employees
    ))


class TestC(SuLoggedTestCase):

    def test_check_if_DBD_boss(self):
        out, err = self.run_cmd_logged_as("whoami", DBD_boss.album())
        self.assertEqual(out, DBD_boss.album())

    def test_step_A(self):
        def is_dbd_worker(emp: Employee) -> bool:
            return emp.dept == SER_DEPT and emp.role != BOSS

        DBD_workers = list(filter(is_dbd_worker, employees))
        self.assertGreater(len(DBD_workers), 0)

        for worker in DBD_workers:
            fpath = f"{ROOT_PATH}/zadania/{worker.id}/{TASK1_FNAME}"

            cmd = f"cat {fpath}"
            out, err = self.run_cmd_logged_as(cmd, DBD_boss.album())
            self.assertEqual(err, '')
            self.assertEqual(out, TASK1_CONTENT)
