from src.employee import Employee, BOSS, BIZ_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from .test_b import TASK1_CONTENT, TASK1_FNAME
from src.main import Dirs
from os.path import join


workers = list(filter(lambda emp: emp.role != BOSS, employees))


class TestF(SuLoggedTestCase):

    def test_f(self):

        for worker in workers:
            worker_dir = join(ROOT_PATH, Dirs.TASKS, worker.id)

            out, err = self.run_cmd_logged_as(f"ls {worker_dir}", worker.album())
            self.assertEqual(err, '')

            fnames = out.split()

            for fname in fnames:
                write_dir: str = None

                if "lokata" in fname:
                    write_dir = join(ROOT_PATH, Dirs.DEPOSITS)
                elif "kredyt" in fname:
                    write_dir = join(ROOT_PATH, Dirs.LOANS)
                else:
                    continue

                out1, err1 = self.run_cmd_logged_as(
                    f"echo 'zrobione' > {join(write_dir, fname)}",
                    worker.album()
                )

                self.assertEqual(out1, '')
                self.assertEqual(err1, '')
