from src.employee import Employee, BOSS, BIZ_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from .test_b import TASK1_CONTENT, TASK1_FNAME

# 1 Ada Abacka dyrektor DBD
DBB_boss, = list(filter(
        lambda emp: emp.role == BOSS and emp.dept == BIZ_DEPT,
        employees
    ))


class TestD(SuLoggedTestCase):

    def test_check_if_DBD_boss(self):
        out, err = self.run_cmd_logged_as("whoami", DBB_boss.album())
        self.assertEqual(out, DBB_boss.album())

    def test_step_A(self):
        def is_dbb_worker(emp: Employee) -> bool:
            return emp.dept == BIZ_DEPT and emp.role != BOSS

        DBB_workers = list(filter(is_dbb_worker, employees))
        self.assertGreater(len(DBB_workers), 0)

        for worker in DBB_workers:
            fpath = f"{ROOT_PATH}/zadania/{worker.id}/{TASK1_FNAME}"

            cmd = f"cat {fpath}"
            out, err = self.run_cmd_logged_as(cmd, DBB_boss.album())
            self.assertEqual(err, '')
            self.assertEqual(out, TASK1_CONTENT)
