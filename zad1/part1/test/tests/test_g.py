from src.employee import Employee, BOSS, BIZ_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from .test_b import TASK1_CONTENT, TASK1_FNAME
from src.main import Dirs
from os.path import join
from typing import List


workers = list(filter(lambda emp: emp.role != BOSS, employees))


class TestG(SuLoggedTestCase):

    def test_g(self):

        loans_dir = join(ROOT_PATH, Dirs.LOANS)

        for worker in workers:
            abs_tasks_path_fnames: List[str] = []
            abs_loans_path_fnames: List[str] = []

            worker_dir = join(ROOT_PATH, Dirs.TASKS, worker.id)

            out1, err1 = self.run_cmd_logged_as(
                f"ls -p {worker_dir} | grep -v /",  # find only files
                worker.album()
            )
            self.assertEqual(err1, '')

            for fname in out1.split():
                abs_tasks_path_fnames.append(join(worker_dir, fname))

            out2, err2 = self.run_cmd_logged_as(
                f"ls -p {loans_dir} | grep -v /",  # find only files
                worker.album()
            )
            self.assertEqual(err2, '')

            for fname in out2.split():
                abs_loans_path_fnames.append(join(loans_dir, fname))

            for path in abs_tasks_path_fnames:
                out3, err3 = self.run_cmd_logged_as(
                    f"echo 'zrobione' >> {path}",
                    worker.album()
                )
                self.assertNotEqual(err3, '')

            for path in abs_loans_path_fnames:
                out3, err3 = self.run_cmd_logged_as(
                    f"echo 'zrobione' >> {path}",
                    worker.album()
                )
                self.assertNotEqual(err3, '')
