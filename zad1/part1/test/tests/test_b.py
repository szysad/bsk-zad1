from src.employee import Employee, BOSS, BIZ_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from typing import List
import subprocess


# 1 Ada Abacka dyrektor DBD
DBB_boss, = list(filter(
        lambda emp: emp.role == BOSS and emp.dept == BIZ_DEPT,
        employees
    ))

TASK1_CONTENT = 'kredyt'
TASK1_FNAME = 'zadanie1.txt'

TASK2_CONTENT = TASK1_CONTENT
TASK2_FNAME = 'zadanie2.txt'

TASK3_CONTENT = 'lokata'
TASK3_FNAME = 'zadanie3.txt'


class TestB(SuLoggedTestCase):

    def test_check_if_DBD_boss(self):
        out, err = self.run_cmd_logged_as("whoami", DBB_boss.album())
        self.assertEqual(out, DBB_boss.album())

    def test_step_A(self):
        def is_dbb_worker(emp: Employee) -> bool:
            return emp.dept == BIZ_DEPT and emp.role != BOSS

        DBD_workers = list(filter(is_dbb_worker, employees))
        self.assertGreater(len(DBD_workers), 0)

        for worker in DBD_workers:
            fpaths = (f"{ROOT_PATH}/zadania/{worker.id}/zadanie{i}.txt" for i in range(1, 4))
            contents = ('kredyt', 'kredyt', 'lokata')

            for content, fpath in zip(contents, fpaths):
                cmd = f"echo {content} > {fpath}"
                out, err = self.run_cmd_logged_as(cmd, DBB_boss.album())
                self.assertEqual(err, '')
