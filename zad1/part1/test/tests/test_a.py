from src.employee import Employee, BOSS, SER_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from typing import List
import subprocess


# 1 Ada Abacka dyrektor DBD
DBD_boss, = list(filter(
        lambda emp: emp.role == BOSS and emp.dept == SER_DEPT,
        employees
    ))

TASK1_CONTENT = 'kredyt'
TASK1_FNAME = 'zadanie1.txt'

TASK2_CONTENT = 'lokata'
TASK2_FNAME = 'zadanie2.txt'


class TestA(SuLoggedTestCase):

    files_to_remove: List[str] = []

    def test_check_if_DBD_boss(self):
        out, err = self.run_cmd_logged_as("whoami", DBD_boss.album())
        self.assertEqual(out, DBD_boss.album())

    def test_step_A(self):
        def is_dbd_worker(emp: Employee) -> bool:
            return emp.dept == SER_DEPT and emp.role != BOSS

        DBD_workers = list(filter(is_dbd_worker, employees))
        self.assertGreater(len(DBD_workers), 0)

        for worker in DBD_workers:
            path1 = f"{ROOT_PATH}/zadania/{worker.id}/zadanie1.txt"
            path2 = f"{ROOT_PATH}/zadania/{worker.id}/zadanie2.txt"

            self.files_to_remove.append(path1)
            self.files_to_remove.append(path2)

            cmd1 = f"echo 'kredyt' > {path1}"
            cmd2 = f"echo 'lokata' > {path2}"

            for cmd in (cmd1, cmd2):
                out, err = self.run_cmd_logged_as(cmd, DBD_boss.album())
                self.assertEqual(err, '')
