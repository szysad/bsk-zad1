from src.employee import Employee, BOSS, BIZ_DEPT
from ..data import employees, ROOT_PATH
from ..suLoggedTestCase import SuLoggedTestCase
from .test_b import TASK1_CONTENT, TASK1_FNAME

# 1 Ada Abacka dyrektor DBD
workers = list(filter(lambda emp: emp.role != BOSS, employees))


class TestE(SuLoggedTestCase):

    def test_e(self):

        for worker in workers:
            worker_dir = f"{ROOT_PATH}/zadania/{worker.id}"

            out, err = self.run_cmd_logged_as(f"ls {worker_dir}", worker.album())
            self.assertEqual(err, '')

            fnames = out.split()

            for fname in fnames:
                out2, err2 = self.run_cmd_logged_as(
                    f"cat {worker_dir}/{fname} > /dev/null",
                    worker.album()
                )

                self.assertEqual(out2, '')
                self.assertEqual(err2, '')

                out3, err3 = self.run_cmd_logged_as(
                    f"echo $'\nzrobione' >> {worker_dir}/{fname}",
                    worker.album()
                )

                self.assertEqual(out3, '')
                self.assertNotEqual(err3, '')
