## usage

### All files are build in `/home`

To run scripts
```bash
$ python3 -m part1.src.main
```

To run tests with already generated files
```bash
$ python3 -m part1.test.main
```
